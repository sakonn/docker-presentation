
# https://github.com/docker-library/python/blob/cb9a39a6c48d4606a68ae8f986373c9c64d430b5/3.8/buster/slim/Dockerfile
FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

ENV FLASK_DEBUG=1
ENV FLASK_APP=app.py

CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0"]

